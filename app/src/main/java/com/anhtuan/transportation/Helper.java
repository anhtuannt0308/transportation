package com.anhtuan.transportation;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ${ADMIN} on 03/18/17.
 */

public class Helper {
	public static String getValueStringFromJSonObject(JSONObject jsonObject, String fieldName) throws JSONException {
		return jsonObject.has(fieldName) ? jsonObject.getString(fieldName) : " ";
	}

	public static long getValueLongFromJSonObject(JSONObject jsonObject, String fieldName) throws JSONException {
		return jsonObject.has(fieldName) ? jsonObject.getLong(fieldName) : 0;
	}
	public static int getValueIntFromJSonObject(JSONObject jsonObject, String fieldName) throws JSONException {
		return jsonObject.has(fieldName) ? jsonObject.getInt(fieldName) : 0;
	}
}
