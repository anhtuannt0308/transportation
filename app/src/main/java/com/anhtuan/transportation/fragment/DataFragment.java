package com.anhtuan.transportation.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.activity.CustomerDetailActivity;
import com.anhtuan.transportation.activity.ItemDetailActivity;
import com.anhtuan.transportation.adapter.DataAdapter;
import com.anhtuan.transportation.information.Customer;
import com.anhtuan.transportation.information.Data;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by ${ADMIN} on 03/14/17.
 */

public class DataFragment extends android.support.v4.app.Fragment implements DataAdapter.OnClickItemData {
	private View view;
	private Calendar calendar;
	private int year, day, month;
	private TextView dateView1, dateView2;
	private ImageView btnDatePicker, btnTimePicker;
	private Button btnXem;
	private RecyclerView rcvItem;
	private DataAdapter dataAdapter;
	private ArrayList<Data> list = new ArrayList<>();
	private String cookie;
	private int mYear, mMonth, mDay;
	private int number = 1;
	public SetDateFromDateTo setDateFromDateTo;
	private int count;

	public void setCount(int count) {
		this.count = count;
	}

	public void setSetDateFromDateTo(SetDateFromDateTo setDateFromDateTo) {
		this.setDateFromDateTo = setDateFromDateTo;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public ArrayList<Data> getList() {
		return list;
	}

	public void setList(ArrayList<Data> list) {
		this.list.clear();
		this.list.addAll(list);
		if (dataAdapter != null) {
			dataAdapter.notifyDataSetChanged();
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_item, container, false);
		initView(list);
		initButtonPage(count);
		return view;
	}

	private void initButtonPage(int count) {
		LinearLayout tbrow = (LinearLayout) view.findViewById(R.id.tbRow_button);
		int pageNumber = 10;
		int x = count / pageNumber;
		int page;
		if (x == 1) {
			page = x;
		} else {
			page = x + 1;
		}
		for (int i = 0; i < page; i++) {
			Button button = new Button(getContext());
			button.setLayoutParams(new LinearLayout.LayoutParams(100, ViewGroup.LayoutParams.WRAP_CONTENT));
			int numbpage = i + 1;
			button.setText("" + numbpage);
			number = i + 1;
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					setDateFromDateTo.setPage("" + number, "10");
				}
			});
			tbrow.addView(button);
		}
	}

	public void initView(ArrayList<Data> listData) {
		btnXem = (Button) view.findViewById(R.id.btn_xem);
		dateView1 = (TextView) view.findViewById(R.id.tv_date_from);
		dateView2 = (TextView) view.findViewById(R.id.tv_date_to);
		btnDatePicker = (ImageView) view.findViewById(R.id.btn_from);
		btnTimePicker = (ImageView) view.findViewById(R.id.btn_to);
		btnDatePicker.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);

				DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
						new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view, int year,
												  int monthOfYear, int dayOfMonth) {
								dateView1.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
							}
						}, mYear, mMonth, mDay);

				datePickerDialog.show();
			}
		});
		btnTimePicker.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final Calendar c = Calendar.getInstance();
				mYear = c.get(Calendar.YEAR);
				mMonth = c.get(Calendar.MONTH);
				mDay = c.get(Calendar.DAY_OF_MONTH);

				DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
						new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view, int year,
												  int monthOfYear, int dayOfMonth) {
								dateView2.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
							}
						}, mYear, mMonth, mDay);
				datePickerDialog.show();

			}
		});
		btnXem.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				setDateFromDateTo.setDate(dateView1.getText().toString(), dateView2.getText().toString());
			}
		});
		calendar = Calendar.getInstance();
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		day = calendar.get(Calendar.DAY_OF_MONTH);
		showDate(year, month + 1, day);
		rcvItem = (RecyclerView) view.findViewById(R.id.rcv_Item);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvItem.setLayoutManager(new GridLayoutManager(getActivity(), 1));
		} else {
			rcvItem.setLayoutManager(new LinearLayoutManager(getActivity()));
		}
		dataAdapter = new DataAdapter(getActivity(), listData);
		dataAdapter.setOnClickItemData(this);
		rcvItem.setAdapter(dataAdapter);
	}

	private void showDate(int year, int i, int day) {
		dateView2.setText(new StringBuilder().append(day).append("-")
				.append(month+1).append("-").append(year));
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void showDetailData(int position) {
		String cookie = getActivity().getIntent().getStringExtra("cookie"); // ??
		Data data = list.get(position);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		String detail = "Tên Hàng Hóa : " + data.getNameItem() + "\n" + "\n" + "Khách Hàng: " + data.getNameCustomer() + "\n" + "\n" + "Vận Chuyển Từ: " + data.getFrom() + "\n" + "\n" + "Vận Chuyển Đến: " + data.getTo() + "\n"
				+ "\n" + "Vị Trí Hiện Tại: " + data.getCurrentLocation() + "\n" + "\n" + "Ngày Nhận Hàng: " + data.getFromDate() + "\n" + "\n" + "Ngày Chuyển Đến: "
				+ data.getFinishDate() + "\n" + "\n" + "Cân Nặng: " + data.getWeight() + "\n" + "\n" + "Kiện Hàng: " + data.getNumPkg() + "\n" + "\n"
				+ "Kiên Hàng Lớn: " + data.getNumBigPkg() + "\n" + "\n" + "Gía Trị Hàng Hóa: " + data.getValue() + "\n" + "\n" + "Cách Trả Phí: " + data.getPayType() + "\n" + "\n"
				+ "Chi Phí KH Trả: " + data.getPrice() + "\n" + "\n" + "Trạng Thái: " + data.getStatus();
		bundle.putString("Titler", "Chi Tiết Đơn Hàng");
		String id = data.getId();
		bundle.putString("id", id);
		bundle.putString("Detail", detail);
		intent.putExtra("bundle", bundle);
		intent.putExtra("cookie", cookie);
		intent.setClass(getActivity(), ItemDetailActivity.class);
		startActivity(intent);
	}

	@Override
	public void showDetailCustomer(int position) {
		Customer customer = list.get(position).getCustomer();
		String id = customer.getId();
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putString("cookie", cookie);
		String detail = "Họ Tên : " + customer.getName() + "\n" + "\n" + "Dư Nợ Hiện Tại: " + customer.getDebt() + "\n" + "\n" + "Điện Thoại: " + customer.getMobile() + "\n" + "\n" + "Vận Chuyển Từ: " + customer.getTransferFrom() + "\n"
				+ "\n" + "Vận Chuyển Đến: " + customer.getTransferTo() + "\n" + "\n" + "Giá Tiền Theo Kilogam (VNĐ/Kg): " + customer.getPricePerKg() + "\n" + "\n" + "Giá tiền theo kiện hàng (VNĐ/kiện): "
				+ customer.getPricePerPackage() + "\n" + "\n" + "Giá Tiền Theo Kiện Hàng Lớn (VNĐ/kiện): " + customer.getPricePerBigPackage() + "\n" + "\n" + "Ghi Chú: " + customer.getNote() + "\n" + "\n";
		bundle.putString("Titler", "Chi Tiết Khách Hàng");
		bundle.putString("Detail", detail);
		bundle.putString("id", id);
		intent.putExtra("bundle", bundle);
		intent.setClass(getActivity(), CustomerDetailActivity.class);
		startActivity(intent);
	}

	public interface SetDateFromDateTo {
		void setDate(String dateFrom, String dateTo);

		void setPage(String page, String numberPage);
	}
}
