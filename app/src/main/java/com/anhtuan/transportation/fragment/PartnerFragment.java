package com.anhtuan.transportation.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.activity.PartnerDetailActivity;
import com.anhtuan.transportation.adapter.PartnerAdapter;
import com.anhtuan.transportation.information.Partner;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/18/17.
 */
public class PartnerFragment extends android.support.v4.app.Fragment implements PartnerAdapter.OnClickItemPartner {
	private View view;
	private RecyclerView rcvPartner;
	private PartnerAdapter partnerAdapter;
	private ArrayList<Partner> listPartner = new ArrayList<>();
	private String cookie;
	private int number = 1;
	private int count;
	private SetCountPartner setCountPartner;

	public void setSetCountPartner(SetCountPartner setCountPartner) {
		this.setCountPartner = setCountPartner;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public void setListPartner(ArrayList<Partner> listPartner) {
		this.listPartner.clear();
		this.listPartner.addAll(listPartner);
		if (partnerAdapter != null) {
			partnerAdapter.notifyDataSetChanged();
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_partner, container, false);
		initView(listPartner);
		initButtonPage(count);
		return view;
	}

	private void initButtonPage(int count) {
		LinearLayout tbrow = (LinearLayout) view.findViewById(R.id.tbRow_buttonpartner);
		int pageNumber = 10;
		int x = count / pageNumber;
		int page;
		if (x == 1) {
			page = x;
		} else {
			page = x + 1;
		}
		for (int i = 0; i < page; i++) {
			Button button = new Button(getContext());
			button.setLayoutParams(new LinearLayout.LayoutParams
					(100, ViewGroup.LayoutParams.WRAP_CONTENT));
			int numbpage = i + 1;
			button.setText("" + numbpage);
			number = i + 1;
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					setCountPartner.setCountPartner("" + number, "10");
				}
			});
			tbrow.addView(button);
		}
	}

	public void initView(ArrayList<Partner> listPartner) {
		rcvPartner = (RecyclerView) view.findViewById(R.id.rcv_partner);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvPartner.setLayoutManager(new GridLayoutManager(getActivity(), 1));
		} else {
			rcvPartner.setLayoutManager(new LinearLayoutManager(getActivity()));
		}
		partnerAdapter = new PartnerAdapter(getActivity(), listPartner);
		partnerAdapter.setOnClickItemPartner(this);
		rcvPartner.setAdapter(partnerAdapter);
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void showDetailPartner(int position) {
		Partner partner = listPartner.get(position);
		String id = partner.getId();
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		String detail = "Họ Tên : " + partner.getName() + "\n" + "\n" + "Dư Nợ Hiện Tại: " + partner.getDebt() + "\n" + "\n" + "Điện Thoại: " + partner.getMobile() + "\n" + "\n" + "Phuowng Tiện Vận Chuyển: " + partner.getVehicle() + "\n"
				+ "\n" + "Gía Tiền Theo Kg: " + partner.getPricePerKg() + "\n" + "\n" + "Giá tiền theo kiện hàng (VNĐ/kiện): "
				+ partner.getPricePerPackage() + "\n" + "\n" + "Giá Tiền Theo Kiện Hàng Lớn (VNĐ/kiện): " + partner.getPricePerBigPackage() + "\n" + "\n" + "Vận chuyển Từ: " + partner.getFrom() + "\n" + "\n" + "Vận Chuyển Đến: " + partner.getTo() + "\n";
		bundle.putString("Titler", "Chi Tiết Đối Tác");
		bundle.putString("Detail", detail);
		bundle.putString("cookie", cookie);
		bundle.putString("id", id);
		intent.putExtra("bundle", bundle);
		intent.setClass(getActivity(), PartnerDetailActivity.class);
		startActivity(intent);
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	public interface SetCountPartner {
		void setCountPartner(String page, String numberPage);
	}
}
