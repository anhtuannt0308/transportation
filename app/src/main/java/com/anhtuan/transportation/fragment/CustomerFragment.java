package com.anhtuan.transportation.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.activity.CustomerDetailActivity;
import com.anhtuan.transportation.adapter.CustomerAdapter;
import com.anhtuan.transportation.information.Customer;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/15/17.
 */

public class CustomerFragment extends android.support.v4.app.Fragment implements CustomerAdapter.OnClickItemCustomer {
	private View view;
	private RecyclerView rcvCustomer;
	private CustomerAdapter customerAdapter;
	ArrayList<Customer> listCustomer=new ArrayList<>();
	private SetCount setCount;String cookie;
	private int number=1;
	private int count;
	public void setSetCount(SetCount setCount) {
		this.setCount = setCount;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public void setListCustomer(ArrayList<Customer> listCustomer) {
		this.listCustomer.clear();
		this.listCustomer.addAll(listCustomer);
		if (customerAdapter != null) {
			customerAdapter.notifyDataSetChanged();
		}
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view=inflater.inflate(R.layout.fragment_customer,container,false);
		initView(listCustomer);
		initButtonPage(count);
		return view;
	}
	private void initButtonPage(int count) {
		LinearLayout tbrow=(LinearLayout)view.findViewById(R.id.tbRow_buttoncustomer);
		int pageNumber=10;
		int x=count/pageNumber;
		int page;
		if(x==1){
			page=x;
		} else{
			page=x+1;}
		for (int i =0;i<page;i++){
			Button button= new Button(getContext());
			button.setLayoutParams(new LinearLayout.LayoutParams
					(100, ViewGroup.LayoutParams.WRAP_CONTENT));
			int numbpage = i + 1;
			button.setText(""+numbpage);
			number=i+1;
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					setCount.setCount(""+number,"10");
				}
			});
			tbrow.addView(button);
		}
	}
	public void initView(ArrayList<Customer> listCustomers){
		rcvCustomer=(RecyclerView)view.findViewById(R.id.rcv_customer);
		int orient = getResources().getConfiguration().orientation;
		if(orient== Configuration.ORIENTATION_PORTRAIT){
			rcvCustomer.setLayoutManager(new GridLayoutManager(getActivity(),1));
		} else{
			rcvCustomer.setLayoutManager(new LinearLayoutManager(getActivity()));
		}
		customerAdapter=new CustomerAdapter(getActivity(),listCustomers);
		customerAdapter.setOnClickItemCustomer(this);
		rcvCustomer.setAdapter(customerAdapter);
	}
	@Override
	public void onResume() {
		super.onResume();

	}
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}


	@Override
	public void showDetailCustomer(int position) {
		Customer customer=listCustomer.get(position);
		String id=customer.getId();
		Intent intent=new Intent();
		Bundle bundle =new Bundle();
		String detail="Họ Tên : "+customer.getName()+"\n"+"\n"+"Dư Nợ Hiện Tại: "+customer.getDebt()+"\n"+"\n"+"Điện Thoại: "+customer.getMobile()+"\n"+"\n"+"Vận Chuyển Từ: "+customer.getTransferFrom()+"\n"
				+"\n"+"Vận Chuyển Đến: "+customer.getTransferTo()+"\n"+"\n"+"Giá Tiền Theo Kilogam (VNĐ/Kg): "+customer.getPricePerKg()+"\n"+"\n"+"Giá tiền theo kiện hàng (VNĐ/kiện): "
				+customer.getPricePerPackage()+"\n"+"\n"+"Giá Tiền Theo Kiện Hàng Lớn (VNĐ/kiện): "+customer.getPricePerBigPackage()+"\n"+"\n"+"Ghi Chú: "+customer.getNote()+"\n"+"\n";
		bundle.putString("Titler","Chi Tiết Khách Hàng");
		bundle.putString("cookie",cookie);
		bundle.putString("Detail",detail);
		bundle.putString("id",id);
		intent.putExtra("bundle",bundle);
		intent.setClass(getActivity(),CustomerDetailActivity.class);
		startActivity(intent);
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}
	public interface  SetCount{
		void setCount(String page, String numberPage);
	}
}
