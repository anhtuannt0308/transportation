package com.anhtuan.transportation.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.activity.WareHouseDetailActivity;
import com.anhtuan.transportation.adapter.WareHouseAdapter;
import com.anhtuan.transportation.information.WareHouse;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/20/17.
 */

public class WareHouseFragment extends Fragment implements WareHouseAdapter.OnClickItemWareHouse {
	private View view;
	private RecyclerView rcvWarehouse;
	private WareHouseAdapter wareHouseAdapter;
	private ArrayList<WareHouse> listWareHouse = new ArrayList<>();
	String cookie;
	private int count;
	private int number = 1;
	private SetCountWareHouse setCountWareHouse;

	public void setSetCountWareHouse(SetCountWareHouse setCountWareHouse) {
		this.setCountWareHouse = setCountWareHouse;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	public void setListWareHouse(ArrayList<WareHouse> listWareHouse) {
		this.listWareHouse.clear();
		this.listWareHouse.addAll(listWareHouse);
		if (wareHouseAdapter != null) {
			wareHouseAdapter.notifyDataSetChanged();
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_warehouse, container, false);
		initView(listWareHouse);
		initButtonPage(count);
		return view;
	}
	private void initButtonPage(int count) {
		LinearLayout tbrow = (LinearLayout) view.findViewById(R.id.tbRow_buttonwarehouse);
		int pageNumber = 10;
		int x = count / pageNumber;
		int page;
		if (x == 1) {
			page = x;
		} else {
			page = x + 1;
		}
		for (int i = 0; i < page; i++) {
			Button button = new Button(getContext());
			button.setLayoutParams(new LinearLayout.LayoutParams
					(100, ViewGroup.LayoutParams.WRAP_CONTENT));
			int numbpage = i + 1;
			button.setText("" + numbpage);
			number = i + 1;
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					setCountWareHouse.setCountWare("" + number, "10");
				}
			});
			tbrow.addView(button);
		}
	}

	public void initView(ArrayList<WareHouse> listWareHouse) {
		rcvWarehouse = (RecyclerView) view.findViewById(R.id.rcv_partner);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvWarehouse.setLayoutManager(new GridLayoutManager(getActivity(), 1));
		} else {
			rcvWarehouse.setLayoutManager(new LinearLayoutManager(getActivity()));
		}
		wareHouseAdapter = new WareHouseAdapter(getActivity(), listWareHouse);
		wareHouseAdapter.setOnClickItemWareHouse(this);
		rcvWarehouse.setAdapter(wareHouseAdapter);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void showDetailWareHouse(int position) {
		WareHouse wareHouse = listWareHouse.get(position);
		String id = wareHouse.getId();
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		String detail = "Họ Tên : " + wareHouse.getName() + "\n" + "\n" + "Dư Nợ Hiện Tại: " + wareHouse.getDebt() + "\n" + "\n" + "Điện Thoại: " + wareHouse.getPhone() + "\n" + "\n" + "Dia Chi: " + wareHouse.getAddrees() + "\n"
				+ "\n";
		bundle.putString("Titler", "Chi Tiết Kho");
		bundle.putString("Detail", detail);
		bundle.putString("id", id);
		bundle.putString("cookie", cookie);
		intent.putExtra("bundle", bundle);
		intent.setClass(getActivity(), WareHouseDetailActivity.class);
		startActivity(intent);
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public interface SetCountWareHouse {
		void setCountWare(String page, String pageNumber);
	}
}
