package com.anhtuan.transportation.information;

/**
 * Created by ${ADMIN} on 03/24/17.
 */

public class WareHouseDebit {
	private String nameCustomer;
	private String employer;
	private String amount;
	private String prepay;

	public WareHouseDebit(String nameCustomer, String employer, String amount, String prepay) {
		this.nameCustomer = nameCustomer;
		this.employer = employer;
		this.amount = amount;
		this.prepay = prepay;
	}

	public String getNameCustomer() {
		return nameCustomer;
	}

	public void setNameCustomer(String nameCustomer) {
		this.nameCustomer = nameCustomer;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPrepay() {
		return prepay;
	}

	public void setPrepay(String prepay) {
		this.prepay = prepay;
	}
}
