package com.anhtuan.transportation.information;

/**
 * Created by ${ADMIN} on 03/20/17.
 */

public class WareHouse {
	private String id;
	private String name;
	private String phone;
	private String addrees;
	private long debt;
	private String note;
	private String fromDate;
	private String updateTime;

	public WareHouse(String id, String name, String phone, String addrees, long debt, String note, String fromDate, String updateTime) {
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.addrees = addrees;
		this.debt = debt;
		this.note = note;
		this.fromDate = fromDate;
		this.updateTime = updateTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddrees() {
		return addrees;
	}

	public void setAddrees(String addrees) {
		this.addrees = addrees;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDebt() {
		return debt;
	}

	public void setDebt(long debt) {
		this.debt = debt;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
