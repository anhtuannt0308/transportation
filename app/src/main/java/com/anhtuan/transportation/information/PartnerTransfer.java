package com.anhtuan.transportation.information;

/**
 * Created by ${ADMIN} on 03/23/17.
 */

public class PartnerTransfer {
	private String weight;
	private String nameCustomer;
	private String fromDate;
	private String toDate;
	private String status;


	public PartnerTransfer(String weight, String nameCustomer, String fromDate, String toDate, String status) {
		this.weight = weight;
		this.nameCustomer = nameCustomer;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getNameCustomer() {
		return nameCustomer;
	}

	public void setNameCustomer(String nameCustomer) {
		this.nameCustomer = nameCustomer;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
}
