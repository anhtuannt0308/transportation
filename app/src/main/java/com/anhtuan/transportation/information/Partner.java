package com.anhtuan.transportation.information;

/**
 * Created by ${ADMIN} on 03/17/17.
 */

public class Partner {
	private String id;
	private String name;
	private String mobile;
	private String from;
	private String to;
	private String status;
	private String debt;
	private String vehicle;
	private String blocked;
	private long pricePerKg;
	private long pricePerPackage;
	private long pricePerBigPackage;
	private long pricePerPercent;

	public Partner(String id, String name, String mobile, String vehicle, String debt, String blocked, long pricePerKg, long pricePerPackage, long pricePerBigPackage, long pricePerPercent, String from, String to, String status) {
		this.id = id;
		this.name = name;
		this.mobile = mobile;
		this.from = from;
		this.to = to;
		this.status = status;
		this.debt = debt;
		this.blocked = blocked;
		this.vehicle = vehicle;
		this.pricePerKg = pricePerKg;
		this.pricePerPackage = pricePerPackage;
		this.pricePerBigPackage = pricePerBigPackage;
		this.pricePerPercent = pricePerPercent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDebt() {
		return debt;
	}

	public void setDebt(String debt) {
		this.debt = debt;
	}

	public String getVehicle() {
		return vehicle;
	}

	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}

	public String isBlocked() {
		return blocked;
	}

	public void setBlocked(String blocked) {
		this.blocked = blocked;
	}

	public long getPricePerKg() {
		return pricePerKg;
	}

	public void setPricePerKg(long pricePerKg) {
		this.pricePerKg = pricePerKg;
	}

	public long getPricePerPackage() {
		return pricePerPackage;
	}

	public void setPricePerPackage(long pricePerPackage) {
		this.pricePerPackage = pricePerPackage;
	}

	public long getPricePerBigPackage() {
		return pricePerBigPackage;
	}

	public void setPricePerBigPackage(long pricePerBigPackage) {
		this.pricePerBigPackage = pricePerBigPackage;
	}

	public long getPricePerPercent() {
		return pricePerPercent;
	}

	public void setPricePerPercent(long pricePerPercent) {
		this.pricePerPercent = pricePerPercent;
	}
}
