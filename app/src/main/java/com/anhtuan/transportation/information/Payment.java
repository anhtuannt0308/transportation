package com.anhtuan.transportation.information;

/**
 * Created by ${ADMIN} on 03/23/17.
 */

public class Payment {
	private String employee;
	private String amount;
	private String type;
	private String currency;
	private String note;
	private String fromDate;

	public Payment(String employee, String amount, String type, String currency, String note, String fromDate) {
		this.employee = employee;
		this.amount = amount;
		this.type = type;
		this.currency = currency;
		this.note = note;
		this.fromDate = fromDate;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
}
