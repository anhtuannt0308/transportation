package com.anhtuan.transportation.information;

import java.io.Serializable;

/**
 * Created by ${ADMIN} on 03/13/17.
 */

public class Data implements Serializable {
	private String id;
	private Customer customer;
	private String nameItem;
	private String nameCustomer;
	private String currentLocation;
	private String fromDate;
	private String finishDate;
	private long weight;
	private int numPkg;
	private int numBigPkg;
	private long value;
	private int payType;
	private long price;
	private String from;
	private String to;
	private String status;

	public Data(String id,Customer customer, String nameItem, String nameCustomer, String currentLocation, String fromDate, String finishDate, long weight, int numPkg, int numBigPkg, long value, int payType, long price, String from, String status, String to) {
		this.id = id;
		this.customer=customer;
		this.nameItem = nameItem;
		this.nameCustomer = nameCustomer;
		this.currentLocation = currentLocation;
		this.fromDate = fromDate;
		this.finishDate = finishDate;
		this.weight = weight;
		this.numPkg = numPkg;
		this.numBigPkg = numBigPkg;
		this.value = value;
		this.payType = payType;
		this.price = price;
		this.from = from;
		this.status = status;
		this.to = to;
	}


	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNameItem() {
		return nameItem;
	}

	public void setNameItem(String nameItem) {
		this.nameItem = nameItem;
	}

	public String getNameCustomer() {
		return nameCustomer;
	}

	public void setNameCustomer(String nameCustomer) {
		this.nameCustomer = nameCustomer;
	}

	public String getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate;
	}

	public long getWeight() {
		return weight;
	}

	public void setWeight(long weight) {
		this.weight = weight;
	}

	public int getNumPkg() {
		return numPkg;
	}

	public void setNumPkg(int numPkg) {
		this.numPkg = numPkg;
	}

	public int getNumBigPkg() {
		return numBigPkg;
	}

	public void setNumBigPkg(int numBigPkg) {
		this.numBigPkg = numBigPkg;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	public int getPayType() {
		return payType;
	}

	public void setPayType(int payType) {
		this.payType = payType;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
