package com.anhtuan.transportation.information;

import java.io.Serializable;

/**
 * Created by ${ADMIN} on 03/13/17.
 */

public class Customer implements Serializable {
	private String id;
	private String name;
	private String mobile;
	private long debt;
	private long pricePerKg;
	private long pricePerPackage;
	private long pricePerBigPackage;
	private long pricePerPercent;
	private String transferFrom;
	private String transferTo;
	private String note;

	public Customer(String id, String name, long debt, String mobile, long pricePerKg, long pricePerPackage, long pricePerBigPackage, long pricePerPercent, String transferFrom, String transferTo, String note) {
		this.id = id;
		this.name = name;
		this.debt = debt;
		this.mobile = mobile;
		this.pricePerKg = pricePerKg;
		this.pricePerPackage = pricePerPackage;
		this.pricePerBigPackage = pricePerBigPackage;
		this.pricePerPercent = pricePerPercent;
		this.transferFrom = transferFrom;
		this.transferTo = transferTo;
		this.note = note;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public long getDebt() {
		return debt;
	}
	public void setDebt(long debt) {
		this.debt = debt;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public long getPricePerKg() {
		return pricePerKg;
	}
	public void setPricePerKg(long pricePerKg) {
		this.pricePerKg = pricePerKg;
	}
	public long getPricePerPackage() {
		return pricePerPackage;
	}
	public void setPricePerPackage(long pricePerPackage) {
		this.pricePerPackage = pricePerPackage;
	}

	public long getPricePerBigPackage() {
		return pricePerBigPackage;
	}

	public void setPricePerBigPackage(long pricePerBigPackage) {
		this.pricePerBigPackage = pricePerBigPackage;
	}

	public long getPricePerPercent() {
		return pricePerPercent;
	}

	public void setPricePerPercent(long pricePerPercent) {
		this.pricePerPercent = pricePerPercent;
	}

	public String getTransferFrom() {
		return transferFrom;
	}

	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}

	public String getTransferTo() {
		return transferTo;
	}

	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}
