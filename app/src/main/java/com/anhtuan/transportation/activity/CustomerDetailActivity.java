package com.anhtuan.transportation.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anhtuan.transportation.Helper;
import com.anhtuan.transportation.R;
import com.anhtuan.transportation.adapter.DataAdapter;
import com.anhtuan.transportation.adapter.PaymentAdapter;
import com.anhtuan.transportation.information.Customer;
import com.anhtuan.transportation.information.Data;
import com.anhtuan.transportation.information.Payment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ${ADMIN} on 03/23/17.
 */

public class CustomerDetailActivity extends Activity implements DataAdapter.OnClickItemData {
	private RecyclerView rcvPayment;
	private RecyclerView rcvListItem;
	private PaymentAdapter paymentAdapter;
	private DataAdapter dataAdapter;
	private ArrayList<Data> listData=new ArrayList<>();
	private ArrayList<Payment> listPayment=new ArrayList<>();
	String cookie;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_detail);
		Bundle bundle= getIntent().getBundleExtra("bundle");
		String customer_id=bundle.getString("id");
		cookie=bundle.getString("cookie");
		String titler=bundle.getString("Titler");
		String detail=bundle.getString("Detail");
		TextView tvTitler=(TextView)findViewById(R.id.tvTitler_customer);
		rcvListItem=(RecyclerView)findViewById(R.id.rcv_Detail_customer_listItem);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvListItem.setLayoutManager(new GridLayoutManager(CustomerDetailActivity.this, 1));
		} else {
			rcvListItem.setLayoutManager(new LinearLayoutManager(this));
		}
		rcvPayment=(RecyclerView)findViewById(R.id.rcv_Detail_customer);
		int orient2 = getResources().getConfiguration().orientation;
		if (orient2 == Configuration.ORIENTATION_PORTRAIT) {
			rcvPayment.setLayoutManager(new GridLayoutManager(CustomerDetailActivity.this, 1));
		} else {
			rcvPayment.setLayoutManager(new LinearLayoutManager(this));
		}
		tvTitler.setText(titler);
		TextView tvDetail=(TextView)findViewById(R.id.tvDetail_customer);
		tvDetail.setText(detail);
		tvDetail.setTextSize(15);
		readJsonMyPartner(customer_id);
		readJsonMyItem(customer_id);
	}
	private void readJsonMyItem(String id) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/hanghoas?customer_id="+id,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("AnhTuan","String"+response);
						try {
							JSONObject root = new JSONObject(response);
							JSONArray listJsonData=root.getJSONArray("data");
							for (int i = 0; i < listJsonData.length(); i++) {
								JSONObject data =listJsonData.getJSONObject(i);
								JSONObject customer=data.getJSONObject("customer");
								String id= Helper.getValueStringFromJSonObject(data,"id");
								long weight = Helper.getValueLongFromJSonObject(data, "weight");
								String nameItem = Helper.getValueStringFromJSonObject(data, "name");
								String nameCustomer=Helper.getValueStringFromJSonObject(customer,"name");
								String from = Helper.getValueStringFromJSonObject(data, "from");
								String to = Helper.getValueStringFromJSonObject(data, "to");
								int numbStatus = Helper.getValueIntFromJSonObject(data, "status");
								String status="";
								if (numbStatus==1){status="Đang Vận Chuyển";}
								if(numbStatus==2){status="Hoàn Thành";}
								if(numbStatus==3){status="Thất Bại";}
								int numPkg=  Helper.getValueIntFromJSonObject(data,"numPkg");
								int numBigPkg=Helper.getValueIntFromJSonObject(data,"numBigPkg");
								long value=Helper.getValueLongFromJSonObject(data,"value");
								int payType=Helper.getValueIntFromJSonObject(data,"payType");
								long price=Helper.getValueLongFromJSonObject(data,"price");
								String fromDate=Helper.getValueStringFromJSonObject(data,"fromDate");
								String finishDate=Helper.getValueStringFromJSonObject(data,"finishDate");
								String currentLocation=Helper.getValueStringFromJSonObject(data,"currentLocation");
								//
								String idCustomer=Helper.getValueStringFromJSonObject(customer,"id");
								String name = Helper.getValueStringFromJSonObject(customer,"name");
								String mobile = Helper.getValueStringFromJSonObject(customer,"mobile");
								long debt = Helper.getValueLongFromJSonObject(customer,"debt");
								long pricePerKg=Helper.getValueLongFromJSonObject(customer,"pricePerKg");
								long pricePerPackage=Helper.getValueLongFromJSonObject(customer,"pricePerPackage");
								long pricePerBigPackage=Helper.getValueLongFromJSonObject(customer,"pricePerBigPackage");
								long pricePerPercent=Helper.getValueLongFromJSonObject(customer,"pricePerPercent");
								String transferFrom=Helper.getValueStringFromJSonObject(customer,"transferFrom");
								String transferTo=Helper.getValueStringFromJSonObject(customer,"transferTo");
								String note=Helper.getValueStringFromJSonObject(customer,"note");
								Customer customerParse=new Customer(idCustomer,name,debt,mobile,pricePerKg,pricePerPackage,pricePerBigPackage,pricePerPercent,transferFrom,transferTo,note);
								Data dataParse = new Data(id,customerParse,nameItem,nameCustomer,currentLocation,fromDate,finishDate,weight,numPkg,numBigPkg,value,payType,price,from,status,to);
								listData.add(dataParse);
							}
							dataAdapter=new DataAdapter(CustomerDetailActivity.this,listData);
							dataAdapter.setOnClickItemData(CustomerDetailActivity.this);
							rcvListItem.setAdapter(dataAdapter);
						} catch (JSONException e) {
							e.printStackTrace();}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie",cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}
	private void readJsonMyPartner(String id) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/thanhtoans?page=1&numberpage=10&id_payer="+id+"&type=2&fromDate=2017-3-1&toDate=2017-3-20%2023:59:59",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Hang Hoa: " + response);
						String s=response;
						try {
							JSONObject root = new JSONObject(response);
							JSONArray listJsonTransport=root.getJSONArray("data");
							for (int i = 0; i < listJsonTransport.length(); i++) {
								JSONObject myPayment = listJsonTransport.getJSONObject(i);
								String employee= Helper.getValueStringFromJSonObject(myPayment,"employee");
								String amount=Helper.getValueStringFromJSonObject(myPayment,"amount");
								String currency=Helper.getValueStringFromJSonObject(myPayment,"currency");
								String note=Helper.getValueStringFromJSonObject(myPayment,"note");
								String fromDate=Helper.getValueStringFromJSonObject(myPayment,"fromDate");
								String type="Thu Phí";
								int numbType=myPayment.getInt("type");
								if(numbType==2){
									type="Trả Phí";
								}
								Payment payment= new Payment(employee,amount,type,currency,note,fromDate);
								listPayment.add(payment);
							}
							paymentAdapter=new PaymentAdapter(CustomerDetailActivity.this,listPayment);
							rcvPayment.setAdapter(paymentAdapter);
						} catch (JSONException e) {
							e.printStackTrace();}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	@Override
	public void showDetailData(int position) {
		String cookie=getIntent().getStringExtra("cookie"); // ??
		Data data=listData.get(position);
		Intent intent=new Intent();
		Bundle bundle =new Bundle();
		String detail="Tên Hàng Hóa : "+data.getNameItem()+"\n"+"\n"+"Khách Hàng: "+data.getNameCustomer()+"\n"+"\n"+"Vận Chuyển Từ: "+data.getFrom()+"\n"+"\n"+"Vận Chuyển Đến: "+data.getTo()+"\n"
				+"\n"+"Vị Trí Hiện Tại: "+data.getCurrentLocation()+"\n"+"\n"+"Ngày Nhận Hàng: "+data.getFromDate()+"\n"+"\n"+"Ngày Chuyển Đến: "
				+data.getFinishDate()+"\n"+"\n"+"Cân Nặng: "+data.getWeight()+"\n"+"\n"+"Kiện Hàng: "+data.getNumPkg()+"\n"+"\n"
				+"Kiên Hàng Lớn: "+data.getNumBigPkg()+"\n"+"\n"+"Gía Trị Hàng Hóa: "+data.getValue()+"\n"+"\n"+"Cách Trả Phí: "+data.getPayType()+"\n"+"\n"
				+"Chi Phí KH Trả: "+data.getPrice()+"\n"+"\n"+"Trạng Thái: "+data.getStatus();
		bundle.putString("Titler","Chi Tiết Đơn Hàng");
		String id=data.getId();
		bundle.putString("id",id);
		bundle.putString("Detail",detail);
		intent.putExtra("bundle",bundle);
		intent.putExtra("cookie",cookie);
		intent.setClass(CustomerDetailActivity.this,ItemDetailActivity.class);
		startActivity(intent);
	}

	@Override
	public void showDetailCustomer(int position) {

	}
}
