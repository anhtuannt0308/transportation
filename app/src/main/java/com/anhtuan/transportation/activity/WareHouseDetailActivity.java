package com.anhtuan.transportation.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anhtuan.transportation.Helper;
import com.anhtuan.transportation.R;
import com.anhtuan.transportation.adapter.PaymentAdapter;
import com.anhtuan.transportation.adapter.WareHouseAdapter;
import com.anhtuan.transportation.adapter.WarehoueDebitAdapter;
import com.anhtuan.transportation.information.Payment;
import com.anhtuan.transportation.information.WareHouseDebit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ${ADMIN} on 03/23/17.
 */

public class WareHouseDetailActivity extends Activity implements WareHouseAdapter.OnClickItemWareHouse{
	private RecyclerView rcvWarehousePayment;
	private RecyclerView rcvWareHouseDebit;
	private PaymentAdapter paymentAdapter;
	private WarehoueDebitAdapter warehoueDebitAdapter;
	private ArrayList<WareHouseDebit> listDebit=new ArrayList<>();
	private ArrayList<Payment> listPayment=new ArrayList<>();
	String cookie;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_warehouse_detail);
		Bundle bundle= getIntent().getBundleExtra("bundle");
		String ware_id=bundle.getString("id");
		String titler=bundle.getString("Titler");
		String detail=bundle.getString("Detail");
		cookie=bundle.getString("cookie");
		TextView tvTitler=(TextView)findViewById(R.id.tvTitler_warehouse);
		rcvWareHouseDebit=(RecyclerView)findViewById(R.id.rcv_Detail_warehouse_debit);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvWareHouseDebit.setLayoutManager(new GridLayoutManager(WareHouseDetailActivity.this, 1));
		} else {
			rcvWareHouseDebit.setLayoutManager(new LinearLayoutManager(this));
		}
		rcvWarehousePayment=(RecyclerView)findViewById(R.id.rcv_Detail_warehouse_payment);
		int orient2 = getResources().getConfiguration().orientation;
		if (orient2 == Configuration.ORIENTATION_PORTRAIT) {
			rcvWarehousePayment.setLayoutManager(new GridLayoutManager(WareHouseDetailActivity.this, 1));
		} else {
			rcvWarehousePayment.setLayoutManager(new LinearLayoutManager(this));
		}
		tvTitler.setText(titler);
		TextView tvDetail=(TextView)findViewById(R.id.tvDetail_Warehouse);
		tvDetail.setText(detail);
		tvDetail.setTextSize(15);
		readJsonDebitWareHouse(ware_id);
		readJsonMyPartner(ware_id);
	}
	private void readJsonDebitWareHouse(String id) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/ghino/"+id,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						try {
							JSONObject root = new JSONObject(response);
							JSONArray listJsonTransport=root.getJSONArray("data");
							for (int i = 0; i < listJsonTransport.length(); i++) {
								JSONObject myPayment = listJsonTransport.getJSONObject(i);
								JSONObject customer=myPayment.getJSONObject("customer");
								String nameCustomer=customer.getString("name");
								String employee=myPayment.getString("employee");
								String amount=myPayment.getString("amount");
								String prepay=myPayment.getString("prePay");
								WareHouseDebit wareHouseDebit=new WareHouseDebit(nameCustomer,employee,amount,prepay);
								listDebit.add(wareHouseDebit);
							}
							warehoueDebitAdapter=new WarehoueDebitAdapter(WareHouseDetailActivity.this,listDebit);
							rcvWareHouseDebit.setAdapter(warehoueDebitAdapter);
						} catch (JSONException e) {
							e.printStackTrace();}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	private void readJsonMyPartner(String id) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/thanhtoans?page=1&numberpage=10&id_payer="+id+"&type=3&fromDate=2017-3-1&toDate=2017-3-20%2023:59:59",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Hang Hoa: " + response);
						try {
							JSONObject root = new JSONObject(response);
							JSONArray listJsonTransport=root.getJSONArray("data");
							for (int i = 0; i < listJsonTransport.length(); i++) {
								JSONObject myPayment = listJsonTransport.getJSONObject(i);
								String employee= Helper.getValueStringFromJSonObject(myPayment,"employee");
								String amount=Helper.getValueStringFromJSonObject(myPayment,"amount");
								String currency=Helper.getValueStringFromJSonObject(myPayment,"currency");
								String note=Helper.getValueStringFromJSonObject(myPayment,"note");
								String fromDate=Helper.getValueStringFromJSonObject(myPayment,"fromDate");
								String type="Thu Phí";
								int numbType=myPayment.getInt("type");
								if(numbType==2){
									type="Trả Phí";
								}
								Payment payment= new Payment(employee,amount,type,currency,note,fromDate);
								listPayment.add(payment);
							}
							paymentAdapter=new PaymentAdapter(WareHouseDetailActivity.this,listPayment);
							rcvWarehousePayment.setAdapter(paymentAdapter);
						} catch (JSONException e) {
							e.printStackTrace();}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	@Override
	public void showDetailWareHouse(int position) {

	}
}
