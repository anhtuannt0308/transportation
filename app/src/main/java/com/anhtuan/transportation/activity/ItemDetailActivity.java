package com.anhtuan.transportation.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anhtuan.transportation.Helper;
import com.anhtuan.transportation.R;
import com.anhtuan.transportation.adapter.PartnerAdapter;
import com.anhtuan.transportation.information.Partner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ${ADMIN} on 03/20/17.
 */

public class ItemDetailActivity extends Activity implements PartnerAdapter.OnClickItemPartner{
	private String item_id;
	private RecyclerView rcvTransfer;
	private PartnerAdapter transferAdapter;
	private ArrayList <Partner>listPartner=new ArrayList<>();
	String cookie;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_detail);
		Bundle bundle= getIntent().getBundleExtra("bundle");
		item_id=bundle.getString("id");
		cookie=bundle.getString("cookie");
		String titler=bundle.getString("Titler");
		String detail=bundle.getString("Detail");
		TextView tvTitler=(TextView)findViewById(R.id.tvTitler);
		rcvTransfer=(RecyclerView)findViewById(R.id.rcv_transport);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvTransfer.setLayoutManager(new GridLayoutManager(ItemDetailActivity.this, 1));
		} else {
			rcvTransfer.setLayoutManager(new LinearLayoutManager(this));
		}
		tvTitler.setText(titler);
		TextView tvDetail=(TextView)findViewById(R.id.tvDetail);
		tvDetail.setText(detail);
		tvDetail.setTextSize(15);
		readJsonMyPartner(item_id);
	}
	private void readJsonMyPartner(String Id) {
			StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/vanchuyens?item_id="+Id,
					new Response.Listener<String>() {
						@Override
						public void onResponse(String response) {
							Log.d("resouce", "Hang Hoa: " + response);
							try {
								JSONObject root = new JSONObject(response);
								JSONArray listJsonTransport=root.getJSONArray("data");
								for (int i = 0; i < listJsonTransport.length(); i++) {
									JSONObject myPartner = listJsonTransport.getJSONObject(i).getJSONObject("partner");
									String namePartner= Helper.getValueStringFromJSonObject(myPartner,"name");
									String id=Helper.getValueStringFromJSonObject(myPartner,"id");
									String debtPartner=Helper.getValueStringFromJSonObject(myPartner,"debt");
									String blocked="";
									Boolean blockedResult=myPartner.getBoolean("blocked");
									if(blockedResult==true){blocked="Đã Khóa";}
									if(blockedResult==false){blocked="Đang Làm Việc";}
									String phonePartner=Helper.getValueStringFromJSonObject(myPartner,"phone");
									String vehiclePartner=Helper.getValueStringFromJSonObject(myPartner,"vehicle");
									long pricePerKg=Helper.getValueLongFromJSonObject(myPartner,"pricePerKg");
									long pricePerPercent=Helper.getValueLongFromJSonObject(myPartner,"pricePerPercent");
									long pricePerPackage=Helper.getValueLongFromJSonObject(myPartner,"pricePerPackage");
									long pricePerBigPackage=Helper.getValueLongFromJSonObject(myPartner,"pricePerBigPackage");
									String status="Đang Làm Việc";
									boolean numbStatus=myPartner.getBoolean("blocked");
									if(numbStatus==true){
										status="Đã Khóa";
									}
									String transferFrom=Helper.getValueStringFromJSonObject(myPartner,"transferFrom");
									String transferTo=Helper.getValueStringFromJSonObject(myPartner,"transferTo");
									Partner partner=new Partner(id,namePartner,phonePartner,vehiclePartner,debtPartner,blocked,pricePerKg,pricePerPackage,pricePerBigPackage,pricePerPercent,transferFrom,transferTo,status);
									listPartner.add(partner);
								}
								int x=listPartner.size();
								transferAdapter=new PartnerAdapter(ItemDetailActivity.this,listPartner);
								transferAdapter.setOnClickItemPartner(ItemDetailActivity.this);
								rcvTransfer.setAdapter(transferAdapter);
							} catch (JSONException e) {
								e.printStackTrace();}
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
						}
					}) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> headers = new HashMap<String, String>();
					headers.put("Cookie", cookie);
					return headers;
				}
			};
			RequestQueue requestQueue = Volley.newRequestQueue(this);
			requestQueue.add(stringRequest);
	}

	@Override
	public void showDetailPartner(int position) {
		Partner transfer=listPartner.get(position);
		String cookie=getIntent().getStringExtra("cookie"); // ??
		Intent intent=new Intent();
		Bundle bundle =new Bundle();
		String detail="Họ Tên : "+transfer.getName()+"\n"+"\n"+"Dư Nợ Còn: "+transfer.getDebt()+"\n"+"\n"+"Số Điện Thoại: "+transfer.getMobile()+"\n"+"\n"+"Phương Tiện Vận Chuyển: "+transfer.getVehicle()+"\n"
				+"\n"+"Gía Tiền Theo Kg: "+transfer.getPricePerKg()+"\n"+"\n"+"Gía Tiền Theo Kiện Hàng (VNĐ): "+transfer.getPricePerPackage()+"\n"+"\n"+"Gía Tiền Theo Kiện Hàng Lớn (VNĐ): "
				+transfer.getPricePerBigPackage()+"\n"+"\n"+"Vận Chuyển Từ: "+transfer.getFrom()+"\n"+"\n"+"Vận Chuyển Đến: "+transfer.getTo()+"\n"+"\n"
				+"Trạng Thái: "+transfer.getStatus()+"\n"+"\n";
		String titler="Chi Tiết Đối Tác";
		bundle.putString("Titler",titler);
		String id=transfer.getId();
		bundle.putString("id",id);
		bundle.putString("Detail",detail);
		intent.putExtra("bundle",bundle);
		intent.putExtra("cookie",cookie);
			intent.setClass(ItemDetailActivity.this,PartnerDetailActivity.class);
		startActivity(intent);
	}
}
