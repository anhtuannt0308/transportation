package com.anhtuan.transportation.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anhtuan.transportation.Helper;
import com.anhtuan.transportation.R;
import com.anhtuan.transportation.adapter.PartnerDetailAdapter;
import com.anhtuan.transportation.adapter.PaymentAdapter;
import com.anhtuan.transportation.information.PartnerTransfer;
import com.anhtuan.transportation.information.Payment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ${ADMIN} on 03/23/17.
 */

public class PartnerDetailActivity extends Activity{
	private RecyclerView rcvPartnerTransfer;
	private RecyclerView rcvPartnerPayment;
	private PartnerDetailAdapter partnerDetailAdapter;
	private PaymentAdapter paymentAdapter;
	private ArrayList<Payment> listPayment=new ArrayList<>();
	private ArrayList<PartnerTransfer> listPartnerTransfer=new ArrayList<>();
	String cookie;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_partner_detail);
		Bundle bundle= getIntent().getBundleExtra("bundle");
		String partner_id=bundle.getString("id");
		cookie=bundle.getString("cookie");
		String titler=bundle.getString("Titler");
		String detail=bundle.getString("Detail");
		TextView tvTitler=(TextView)findViewById(R.id.tvTitler_partner);
		rcvPartnerTransfer=(RecyclerView)findViewById(R.id.rcv_Detail_partnerTransfer);
		int orient = getResources().getConfiguration().orientation;
		if (orient == Configuration.ORIENTATION_PORTRAIT) {
			rcvPartnerTransfer.setLayoutManager(new GridLayoutManager(PartnerDetailActivity.this, 1));
		} else {
			rcvPartnerTransfer.setLayoutManager(new LinearLayoutManager(this));
		}
		rcvPartnerPayment=(RecyclerView)findViewById(R.id.rcv_Detail_partnerPayment);
		int orient2 = getResources().getConfiguration().orientation;
		if (orient2 == Configuration.ORIENTATION_PORTRAIT) {
			rcvPartnerPayment.setLayoutManager(new GridLayoutManager(PartnerDetailActivity.this, 1));
		} else {
			rcvPartnerPayment.setLayoutManager(new LinearLayoutManager(this));
		}
		tvTitler.setText(titler);
		TextView tvDetail=(TextView)findViewById(R.id.tvDetail_Partner);
		tvDetail.setText(detail);
		tvDetail.setTextSize(15);
		readJsonTransportMyPartner(partner_id);
		readJsonMyPartner(partner_id);
	}
	private void readJsonTransportMyPartner(String id) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/vanchuyens?partner_id="+id,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce1", "Hang Hoa: " + response);
						try {
							JSONObject root = new JSONObject(response);
							JSONArray listJsonTransport=root.getJSONArray("data");
							for (int i = 0; i < listJsonTransport.length(); i++) {
								JSONObject myPayment = listJsonTransport.getJSONObject(i);
								JSONObject item=myPayment.getJSONObject("item");
								JSONObject customer=item.getJSONObject("customer");
								String weight=Helper.getValueStringFromJSonObject(item,"weight");
								String nameCustomer= Helper.getValueStringFromJSonObject(customer,"name");
								String fromDate=myPayment.getString("fromDate");
								String toDate=myPayment.getString("finishDate");
								String status="Dang Van Chuyen";
								if(myPayment.getInt("status")==2){
									status="Da Hoan Thanh";
								}
								PartnerTransfer partnerTransfer=new PartnerTransfer(weight,nameCustomer,fromDate,toDate,status);
								listPartnerTransfer.add(partnerTransfer);
							}
							partnerDetailAdapter=new PartnerDetailAdapter(PartnerDetailActivity.this,listPartnerTransfer);
							rcvPartnerTransfer.setAdapter(partnerDetailAdapter);
						} catch (JSONException e) {
							e.printStackTrace();}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}
	private void readJsonMyPartner(String id) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/thanhtoans?page=1&numberpage=10&id_payer="+id+"&type=1&fromDate=2017-3-1&toDate=2017-3-24%2023:59:59",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Hang Hoa: " + response);
						try {
							JSONObject root = new JSONObject(response);
							JSONArray listJsonTransport=root.getJSONArray("data");
							for (int i = 0; i < listJsonTransport.length(); i++) {
								JSONObject myPayment = listJsonTransport.getJSONObject(i);
								String employee= Helper.getValueStringFromJSonObject(myPayment,"employee");
								String amount=Helper.getValueStringFromJSonObject(myPayment,"amount");
								String currency=Helper.getValueStringFromJSonObject(myPayment,"currency");
								String note=Helper.getValueStringFromJSonObject(myPayment,"note");
								String fromDate=Helper.getValueStringFromJSonObject(myPayment,"fromDate");
								String type="Thu Phí";
								int numbType=myPayment.getInt("type");
								if(numbType==2){
									type="Trả Phí";
								}
								Payment payment= new Payment(employee,amount,type,currency,note,fromDate);
								listPayment.add(payment);
							}
							paymentAdapter=new PaymentAdapter(PartnerDetailActivity.this,listPayment);
							rcvPartnerPayment.setAdapter(paymentAdapter);
						} catch (JSONException e) {
							e.printStackTrace();}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

}
