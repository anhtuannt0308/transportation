package com.anhtuan.transportation.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anhtuan.transportation.Helper;
import com.anhtuan.transportation.R;
import com.anhtuan.transportation.adapter.ViewPagerAdapter;
import com.anhtuan.transportation.fragment.CustomerFragment;
import com.anhtuan.transportation.fragment.DataFragment;
import com.anhtuan.transportation.fragment.PartnerFragment;
import com.anhtuan.transportation.fragment.WareHouseFragment;
import com.anhtuan.transportation.information.Customer;
import com.anhtuan.transportation.information.Data;
import com.anhtuan.transportation.information.Partner;
import com.anhtuan.transportation.information.WareHouse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements DataFragment.SetDateFromDateTo,CustomerFragment.SetCount,PartnerFragment.SetCountPartner,WareHouseFragment.SetCountWareHouse {
	private DataFragment dataFragment;
	private CustomerFragment customerFragment;
	private PartnerFragment partnerFragment;
	private WareHouseFragment wareHouseFragment;
	private ViewPager viewPager;
	private ViewPagerAdapter viewPagerAdapter;
	private TabLayout tabLayout;
	private ArrayList<Data> listData = new ArrayList<>();
	private ArrayList<Customer> listCustomer=new ArrayList<>();
	private ArrayList<Partner> listPartner=new ArrayList<>();
	private ArrayList<WareHouse> listWareHouse=new ArrayList<>();
	private String cookie;
	private String dayFrom = "2017-3-1";
	private String dayTo = "2017-3-24";
	private String page="1";
	private String numberPage="10";
	private int countData,countCustomer,countPartner,countWarehouse;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		cookie = getIntent().getStringExtra("cookie");
		tabLayout = (TabLayout) findViewById(R.id.main_tablayout);
		viewPager = (ViewPager) findViewById(R.id.main_viewpager);
		viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(viewPagerAdapter);
		tabLayout.setupWithViewPager(viewPager);
		TextView textViewLogout = (TextView) findViewById(R.id.logout);
		textViewLogout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				cookie = "";
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, LoginActivity.class);

				startActivity(intent);
				finish();
			}
		});

	}

	public void initDataToFragmentData(ArrayList<Data> listData,int countData) {
		if (dataFragment == null) {
			dataFragment = new DataFragment();
			dataFragment.setList(listData);
			dataFragment.setCount(countData);
			dataFragment.setSetDateFromDateTo(this);
			viewPagerAdapter.addFragment(dataFragment, "Data");
		} else {
			dataFragment.setList(listData);
			dataFragment.setCount(countData);
			dataFragment.setSetDateFromDateTo(this);
		}
		dataFragment.setCookie(cookie);
		viewPagerAdapter.notifyDataSetChanged();
	}

	public void initDataToFragmentCustomer(ArrayList<Customer> listCustomer,int countCustomer) {
		if (customerFragment == null) {
			customerFragment = new CustomerFragment();
			customerFragment.setListCustomer(listCustomer);
			customerFragment.setCount(countCustomer);
			customerFragment.setSetCount(this);
			viewPagerAdapter.addFragment(customerFragment, "Customer");
		} else {
			customerFragment.setListCustomer(listCustomer);
			customerFragment.setCount(countCustomer);
			customerFragment.setSetCount(this);
		}
		customerFragment.setCookie(cookie);
		viewPagerAdapter.notifyDataSetChanged();
	}

	public void initDataToFragmentPartner(ArrayList<Partner> listPartner,int countPartner) {
		if(partnerFragment==null){
			partnerFragment = new PartnerFragment();
			partnerFragment.setListPartner(listPartner);
			partnerFragment.setCount(countPartner);
			partnerFragment.setSetCountPartner(this);
			viewPagerAdapter.addFragment(partnerFragment, "Partner");
		} else{
			partnerFragment.setListPartner(listPartner);
			partnerFragment.setCount(countPartner);
			partnerFragment.setSetCountPartner(this);
		}
		partnerFragment.setCookie(cookie);
		viewPagerAdapter.notifyDataSetChanged();
	}

	public void initDataToFragmentWareHouse(ArrayList<WareHouse> listWareHouse,int countWarehouse) {
		if(wareHouseFragment==null){
			wareHouseFragment = new WareHouseFragment();
			wareHouseFragment.setListWareHouse(listWareHouse);
			wareHouseFragment.setCount(countWarehouse);
			wareHouseFragment.setSetCountWareHouse(this);
			viewPagerAdapter.addFragment(wareHouseFragment, "WareHouse");
		}else {
			wareHouseFragment.setListWareHouse(listWareHouse);
			wareHouseFragment.setCount(countWarehouse);
			wareHouseFragment.setSetCountWareHouse(this);
		}
		wareHouseFragment.setCookie(cookie);
		viewPagerAdapter.notifyDataSetChanged();
	}

	public void readJsonData(String dayFrom, String dayTo,String page,String numberPage) {
		this.dayFrom = dayFrom;
		this.dayTo = dayTo;
		this.page=page;
		this.numberPage=numberPage;
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/hanghoas?page="+page+"&numberpage="+numberPage+"&fromDate=" + dayFrom + "&toDate=" + dayTo + "%2023:59:59",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Hang Hoa: " + response);
						listData.clear();
						try {
							JSONObject root = new JSONObject(response);
							countData=root.getInt("count");
							JSONArray listJsonData = root.getJSONArray("data");
							for (int i = 0; i < listJsonData.length(); i++) {
								JSONObject data = listJsonData.getJSONObject(i);
								JSONObject customer = data.getJSONObject("customer");
								String id = Helper.getValueStringFromJSonObject(data, "id");
								long weight = Helper.getValueLongFromJSonObject(data, "weight");
								String nameItem = Helper.getValueStringFromJSonObject(data, "name");
								String nameCustomer = Helper.getValueStringFromJSonObject(customer, "name");
								String from = Helper.getValueStringFromJSonObject(data, "from");
								String to = Helper.getValueStringFromJSonObject(data, "to");
								int numbStatus = Helper.getValueIntFromJSonObject(data, "status");
								String status = "";
								if (numbStatus == 1) {
									status = "Đang Vận Chuyển";
								}
								if (numbStatus == 2) {
									status = "Hoàn Thành";
								}
								if (numbStatus == 3) {
									status = "Thất Bại";
								}
								int numPkg = Helper.getValueIntFromJSonObject(data, "numPkg");
								int numBigPkg = Helper.getValueIntFromJSonObject(data, "numBigPkg");
								long value = Helper.getValueLongFromJSonObject(data, "value");
								int payType = Helper.getValueIntFromJSonObject(data, "payType");
								long price = Helper.getValueLongFromJSonObject(data, "price");
								String fromDate = Helper.getValueStringFromJSonObject(data, "fromDate");
								String finishDate = Helper.getValueStringFromJSonObject(data, "finishDate");
								String currentLocation = Helper.getValueStringFromJSonObject(data, "currentLocation");
								//
								String idCustomer = Helper.getValueStringFromJSonObject(customer, "id");
								String name = Helper.getValueStringFromJSonObject(customer, "name");
								String mobile = Helper.getValueStringFromJSonObject(customer, "mobile");
								long debt = Helper.getValueLongFromJSonObject(customer, "debt");
								long pricePerKg = Helper.getValueLongFromJSonObject(customer, "pricePerKg");
								long pricePerPackage = Helper.getValueLongFromJSonObject(customer, "pricePerPackage");
								long pricePerBigPackage = Helper.getValueLongFromJSonObject(customer, "pricePerBigPackage");
								long pricePerPercent = Helper.getValueLongFromJSonObject(customer, "pricePerPercent");
								String transferFrom = Helper.getValueStringFromJSonObject(customer, "transferFrom");
								String transferTo = Helper.getValueStringFromJSonObject(customer, "transferTo");
								String note = Helper.getValueStringFromJSonObject(customer, "note");
								Customer customerParse = new Customer(idCustomer, name, debt, mobile, pricePerKg, pricePerPackage, pricePerBigPackage, pricePerPercent, transferFrom, transferTo, note);
								Data dataParse = new Data(id, customerParse, nameItem, nameCustomer, currentLocation, fromDate, finishDate, weight, numPkg, numBigPkg, value, payType, price, from, status, to);
								listData.add(dataParse);
							}
							initDataToFragmentData(listData,countData);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	public void readJsonCustomer(String page,String numberPage) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/khachhangs?page="+page+"&numberpage="+numberPage+"10",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Khach Hang: " + response);
						try {
							listCustomer.clear();
							JSONObject root = new JSONObject(response);
							countCustomer=root.getInt("count");
							JSONArray listJsonData = root.getJSONArray("data");
							for (int i = 0; i < listJsonData.length(); i++) {
								JSONObject customer = listJsonData.getJSONObject(i);
								String id = Helper.getValueStringFromJSonObject(customer, "id");
								String name = Helper.getValueStringFromJSonObject(customer, "name");
								String mobile = Helper.getValueStringFromJSonObject(customer, "mobile");
								long debt = Helper.getValueLongFromJSonObject(customer, "debt");
								long pricePerKg = Helper.getValueLongFromJSonObject(customer, "pricePerKg");
								long pricePerPackage = Helper.getValueLongFromJSonObject(customer, "pricePerPackage");
								long pricePerBigPackage = Helper.getValueLongFromJSonObject(customer, "pricePerBigPackage");
								long pricePerPercent = Helper.getValueLongFromJSonObject(customer, "pricePerPercent");
								String transferFrom = Helper.getValueStringFromJSonObject(customer, "transferFrom");
								String transferTo = Helper.getValueStringFromJSonObject(customer, "transferTo");
								String note = Helper.getValueStringFromJSonObject(customer, "note");
								Customer customerParse = new Customer(id, name, debt, mobile, pricePerKg, pricePerPackage, pricePerBigPackage, pricePerPercent, transferFrom, transferTo, note);
								listCustomer.add(customerParse);
							}
							initDataToFragmentCustomer(listCustomer,countCustomer);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	public void readJsonPartner(String page,String numberPage) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/doitacs?page=\"+page+\"&numberpage=\"+numberPage+\"10\"",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Doi Tac: " + response);
						try {
							listPartner.clear();
							JSONObject root = new JSONObject(response);
							countPartner=root.getInt("count");
							JSONArray listJsonData = root.getJSONArray("data");
							for (int i = 0; i < listJsonData.length(); i++) {
								JSONObject partner = listJsonData.getJSONObject(i);
								String id = Helper.getValueStringFromJSonObject(partner, "id");
								String name = Helper.getValueStringFromJSonObject(partner, "name");
								String mobile = Helper.getValueStringFromJSonObject(partner, "phone");
								String vehicle = Helper.getValueStringFromJSonObject(partner, "vehicle");
								String debt = Helper.getValueStringFromJSonObject(partner, "debt");
								String blocked = "";
								Boolean blockedResult = partner.getBoolean("blocked");
								if (blockedResult == true) {
									blocked = "Đã Khóa";
								}
								if (blockedResult == false) {
									blocked = "Đang Làm Việc";
								}
								long pricePerKg = Helper.getValueLongFromJSonObject(partner, "pricePerKg");
								long pricePerPackage = Helper.getValueLongFromJSonObject(partner, "pricePerPackage");
								long pricePerBigPackage = Helper.getValueLongFromJSonObject(partner, "pricePerBigPackage");
								long pricePerPercent = Helper.getValueLongFromJSonObject(partner, "pricePerPercent");
								String transferFrom = Helper.getValueStringFromJSonObject(partner, "transferFrom");
								String transferTo = Helper.getValueStringFromJSonObject(partner, "transferTo");
								String status = Helper.getValueStringFromJSonObject(partner, "note");
								Partner partner1 = new Partner(id, name, mobile, vehicle, debt, blocked, pricePerKg, pricePerPackage, pricePerBigPackage, pricePerPercent, transferFrom, transferTo, status);
								listPartner.add(partner1);
							}
							initDataToFragmentPartner(listPartner,countPartner);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	public void readJsonWareHouse(String page,String numberPage) {
		StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://transportation.techlead.vn/api/chukhos?page=\"+page+\"&numberpage=\"+numberPage+\"10\"",
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.d("resouce", "Kho: " + response);
						try {
							listWareHouse.clear();
							JSONObject root = new JSONObject(response);
							countWarehouse=root.getInt("count");
							JSONArray listJsonData = root.getJSONArray("data");
							for (int i = 0; i < listJsonData.length(); i++) {
								JSONObject warehouse = listJsonData.getJSONObject(i);
								String id = Helper.getValueStringFromJSonObject(warehouse, "id");
								String name = Helper.getValueStringFromJSonObject(warehouse, "name");
								long debt = Helper.getValueLongFromJSonObject(warehouse, "debt");
								String mobile = Helper.getValueStringFromJSonObject(warehouse, "phone");
								String address = Helper.getValueStringFromJSonObject(warehouse, "address");
								String note = Helper.getValueStringFromJSonObject(warehouse, "note");
								String fromDate = Helper.getValueStringFromJSonObject(warehouse, "fromDate");
								String updateTime = Helper.getValueStringFromJSonObject(warehouse, "updateTime");
								WareHouse wareHouse1 = new WareHouse(id, name, mobile, address, debt, note, fromDate, updateTime);
								listWareHouse.add(wareHouse1);
							}
							initDataToFragmentWareHouse(listWareHouse,countWarehouse);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> headers = new HashMap<String, String>();
				headers.put("Cookie", cookie);
				return headers;
			}
		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}

	@Override
	public void setDate(String dateFrom, String dateTo) {
		dayFrom = dateFrom;
		dayTo = dateTo;
		readJsonData(dayFrom,dayTo,page,numberPage);
	}

	@Override
	public void setPage(String page, String numberPage) {
		this.page=page;
		this.numberPage=numberPage;
		readJsonData(dayFrom,dayTo,page,numberPage);
	}
	@Override
	protected void onResume() {
		super.onResume();
		readJsonData(dayFrom, dayTo,page,numberPage);
		readJsonCustomer(page,numberPage);
		readJsonPartner(page,numberPage);
		readJsonWareHouse(page,numberPage);
	}

	@Override
	public void setCount(String page, String numberPage) {
		this.page=page;
		this.numberPage=numberPage;
	}

	@Override
	public void setCountPartner(String page, String numberPage) {
		this.page=page;
		this.numberPage=numberPage;
	}

	@Override
	public void setCountWare(String page, String pageNumber) {
		this.page=page;
		numberPage=pageNumber;
	}
}