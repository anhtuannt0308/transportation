package com.anhtuan.transportation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.anhtuan.transportation.R;

import java.util.Map;

public class LoginActivity extends AppCompatActivity {
	EditText edtUser, edtPass;
	String name, pass;
	private String cookie;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		edtUser = (EditText) findViewById(R.id.edt_username);
		edtPass = (EditText) findViewById(R.id.edt_password);
	}

	public void login(View v) {
		name = edtUser.getText().toString().trim();
		pass = edtPass.getText().toString().trim();
		String url = "http://transportation.techlead.vn/api/login?user=" + name + "&password=" + pass;
		StringRequest stringRequest = new StringRequest(Request.Method.POST,
				url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						if (!response.trim().contains("401")) {
							Toast.makeText(LoginActivity.this, "Đăng Nhập Thành Công", Toast.LENGTH_SHORT).show();
							Intent intent = new Intent();
							intent.setClass(LoginActivity.this, MainActivity.class);
							intent.putExtra("cookie", cookie);
							startActivity(intent);
						} else {
							Toast.makeText(LoginActivity.this, "Sai Tài Khoản Hoặc Mật Khẩu", Toast.LENGTH_SHORT).show();
						}
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
					}
				}) {
			@Override
			protected Response<String> parseNetworkResponse(NetworkResponse response) {
				Map<String, String> responseHeaders = response.headers;
				String rawCookies = responseHeaders.get("Set-Cookie");
				cookie = rawCookies.split(";")[0];
				return super.parseNetworkResponse(response);
			}

		};
		RequestQueue requestQueue = Volley.newRequestQueue(this);
		requestQueue.add(stringRequest);
	}
}
