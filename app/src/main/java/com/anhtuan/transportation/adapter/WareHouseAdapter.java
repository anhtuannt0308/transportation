package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.WareHouse;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/20/17.
 */

public class WareHouseAdapter extends RecyclerView.Adapter {
	private Context mContext;
	private ArrayList<WareHouse> listWarehouse;
	public OnClickItemWareHouse onClickItemWare;

	public void setOnClickItemWareHouse(OnClickItemWareHouse onClickItemWare) {
		this.onClickItemWare = onClickItemWare;
	}

	public WareHouseAdapter(Context mContext, ArrayList<WareHouse> listWarehouse) {
		this.mContext = mContext;
		this.listWarehouse = listWarehouse;
	}

	private class ViewHolder extends RecyclerView.ViewHolder {
		private TextView tvName, tvPhoneNumber, tvAddress, tvDebit;
		private LinearLayout linearLayout;

		public ViewHolder(View itemView) {
			super(itemView);
			linearLayout = (LinearLayout) itemView.findViewById(R.id.ln_item_warehouse);
			tvName = (TextView) itemView.findViewById(R.id.tv_warehouse_name);
			tvPhoneNumber = (TextView) itemView.findViewById(R.id.tv_warehouse_mobile);
			tvAddress = (TextView) itemView.findViewById(R.id.tv_warehouse_address);
			tvDebit = (TextView) itemView.findViewById(R.id.tv_warehouse_debit);
			linearLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickItemWare.showDetailWareHouse(getAdapterPosition());
				}
			});
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.item_warehouse, parent, false);
		RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
		layoutParams.setMargins(0, 2, 0, 2);
		view.setLayoutParams(layoutParams);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		final ViewHolder viewHolder = (WareHouseAdapter.ViewHolder) holder;
		viewHolder.tvName.setText(listWarehouse.get(position).getName());
		viewHolder.tvPhoneNumber.setText(listWarehouse.get(position).getPhone());
		viewHolder.tvAddress.setText(listWarehouse.get(position).getAddrees());
		viewHolder.tvDebit.setText(String.valueOf(listWarehouse.get(position).getDebt()));
	}

	@Override
	public int getItemCount() {
		return listWarehouse.size();
	}

	public interface OnClickItemWareHouse {
		void showDetailWareHouse(int position);
	}
}
