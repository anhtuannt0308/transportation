package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.WareHouseDebit;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/24/17.
 */

public class WarehoueDebitAdapter extends RecyclerView.Adapter {
	private Context mContext;
	private ArrayList<WareHouseDebit> listWarehouse;

	public WarehoueDebitAdapter(Context mContext, ArrayList<WareHouseDebit> listWarehouse) {
		this.mContext = mContext;
		this.listWarehouse = listWarehouse;
	}
	private class ViewHolder extends RecyclerView.ViewHolder {
		private TextView tvName, tvEmployer, tvAmount, tvprePay;

		public ViewHolder(View itemView) {
			super(itemView);
			tvName = (TextView) itemView.findViewById(R.id.tv_ware_debt_name);
			tvEmployer = (TextView) itemView.findViewById(R.id.tv_ware_debt_employee);
			tvAmount = (TextView) itemView.findViewById(R.id.tv_ware_debt_amount);
			tvprePay = (TextView) itemView.findViewById(R.id.tv_ware_debt_prepay);
		}
	}
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.item_debit_warehouse, parent, false);
		RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
		layoutParams.setMargins(0, 2, 0, 2);
		view.setLayoutParams(layoutParams);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		ViewHolder viewHolder = (ViewHolder) holder;
		viewHolder.tvName.setText(listWarehouse.get(position).getNameCustomer());
		viewHolder.tvEmployer.setText(listWarehouse.get(position).getEmployer());
		viewHolder.tvAmount.setText(listWarehouse.get(position).getAmount());
		viewHolder.tvprePay.setText(String.valueOf(listWarehouse.get(position).getPrepay()));
	}

	@Override
	public int getItemCount() {
		return listWarehouse.size();
	}
}
