package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.Customer;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/15/17.
 */

public class CustomerAdapter extends RecyclerView.Adapter{
	private Context mContext;
	private ArrayList<Customer> listCustomer;
	public CustomerAdapter.OnClickItemCustomer onClickItemCustomer;
	public void setOnClickItemCustomer(CustomerAdapter.OnClickItemCustomer onClickItemCustomer){
		this.onClickItemCustomer=onClickItemCustomer;
	}
	public CustomerAdapter(Context mContext,ArrayList<Customer> listCustomer) {
		this.mContext = mContext;
		this.listCustomer=listCustomer;
	}
private  class ViewHolder extends RecyclerView.ViewHolder{

	private TextView tvName,tvPhoneNumber,tvDebit;
	private LinearLayout lnItemCustomer;
	public ViewHolder(View itemView) {
		super(itemView);
		tvName=(TextView)itemView.findViewById(R.id.tv_name_customer);
		tvPhoneNumber=(TextView)itemView.findViewById(R.id.tv_phone_customer);
		tvDebit=(TextView)itemView.findViewById(R.id.tv_debit_customer);
		lnItemCustomer=(LinearLayout)itemView.findViewById(R.id.ln_ItemCustomer);
		lnItemCustomer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickItemCustomer.showDetailCustomer(getAdapterPosition());
			}
		});
	}
}
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view= LayoutInflater.from(mContext).inflate(R.layout.item_customer,parent,false);
		RecyclerView.LayoutParams layoutParams=new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150);
		layoutParams.setMargins(0,2,0,2);
		view.setLayoutParams(layoutParams);
		return new ViewHolder(view);
	}
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		final ViewHolder viewHolder= (ViewHolder) holder;
		viewHolder.tvName.setText(listCustomer.get(position).getName());
		viewHolder.tvPhoneNumber.setText(listCustomer.get(position).getMobile());
		viewHolder.tvDebit.setText(String.valueOf(listCustomer.get(position).getDebt()));
	}
	@Override
	public int getItemCount() {
		return listCustomer.size();
	}
	public interface OnClickItemCustomer{
		void showDetailCustomer(int position);
	}
}
