package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.PartnerTransfer;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/23/17.
 */

public class PartnerDetailAdapter extends RecyclerView.Adapter {
	private Context mContext;
	private ArrayList<PartnerTransfer> listPartner;

	public PartnerDetailAdapter(Context mContext, ArrayList<PartnerTransfer> listPartner) {
		this.mContext = mContext;
		this.listPartner = listPartner;
	}

	private class ViewHolder extends RecyclerView.ViewHolder {
		private TextView tvKg, tvNameCustomer, tvFromDate, tvToDate, tvStatus;

		public ViewHolder(View itemView) {
			super(itemView);
			tvKg = (TextView) itemView.findViewById(R.id.tv_transfer_name);
			tvNameCustomer = (TextView) itemView.findViewById(R.id.tv_transfer_mobile);
			tvFromDate = (TextView) itemView.findViewById(R.id.tv_transfer_from);
			tvToDate = (TextView) itemView.findViewById(R.id.tv_transfer_to);
			tvStatus = (TextView) itemView.findViewById(R.id.tv_transfer_status);
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.item_transfer, parent, false);
		RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
		layoutParams.setMargins(0, 2, 0, 2);
		view.setLayoutParams(layoutParams);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		ViewHolder viewHolder = (ViewHolder) holder;
		viewHolder.tvKg.setText(listPartner.get(position).getWeight());
		viewHolder.tvNameCustomer.setText(listPartner.get(position).getNameCustomer());
		viewHolder.tvFromDate.setText(listPartner.get(position).getFromDate());
		viewHolder.tvToDate.setText(listPartner.get(position).getToDate());
		viewHolder.tvStatus.setText(listPartner.get(position).getStatus());
	}

	@Override
	public int getItemCount() {
		return listPartner.size();
	}
}
