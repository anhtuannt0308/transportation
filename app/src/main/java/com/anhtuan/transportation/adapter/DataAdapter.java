package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.Data;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/14/17.
 */

public class DataAdapter extends RecyclerView.Adapter {
	private Context mContext;
	private ArrayList<Data> listDatas;
	public OnClickItemData onClickItemData;
	public void setOnClickItemData(OnClickItemData onClickItemData){
		this.onClickItemData=onClickItemData;
	}
	public DataAdapter(Context mContext, ArrayList<Data>listDatas) {
		this.mContext=mContext;
		this.listDatas=listDatas;
	}
	public class ViewHolder extends RecyclerView.ViewHolder{
		private TextView tvKg,tvCustomer,tvFrom,tvTo,tvStatus;
		public ViewHolder(View itemView) {
			super(itemView);
			tvKg=(TextView)itemView.findViewById(R.id.tv_item_kg);
			tvCustomer=(TextView)itemView.findViewById(R.id.tv_item_customer);
			tvFrom=(TextView)itemView.findViewById(R.id.tv_item_from);
			tvTo=(TextView)itemView.findViewById(R.id.tv_item_to);
			tvStatus=(TextView)itemView.findViewById(R.id.tv_item_status);
			tvKg.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickItemData.showDetailData(getAdapterPosition());
				}
			});
			tvCustomer.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickItemData.showDetailCustomer(getAdapterPosition());
				}
			});
		}
	}
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view= LayoutInflater.from(mContext).inflate(R.layout.item_data,parent,false);
		RecyclerView.LayoutParams layoutParams=new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150);
		layoutParams.setMargins(0,2,0,2);
		view.setLayoutParams(layoutParams);
		return new ViewHolder (view);
	}
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		final ViewHolder viewHolder=(ViewHolder)holder;
		viewHolder.tvKg.setText(String.valueOf(listDatas.get(position).getWeight()));
		viewHolder.tvCustomer.setText(listDatas.get(position).getNameItem());
		viewHolder.tvFrom.setText(listDatas.get(position).getFrom());
		viewHolder.tvTo.setText(listDatas.get(position).getTo());
		viewHolder.tvStatus.setText(String.valueOf(listDatas.get(position).getStatus()));
	}
	@Override
	public int getItemCount() {
		return listDatas.size();
	}
	public interface OnClickItemData{
		void showDetailData(int position);

		void showDetailCustomer(int position);
	}
}
