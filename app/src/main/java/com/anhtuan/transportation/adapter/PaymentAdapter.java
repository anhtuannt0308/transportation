package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.Payment;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/23/17.
 */
public class PaymentAdapter extends RecyclerView.Adapter {
	private Context mContext;
	private ArrayList<Payment> listPayment;

	public PaymentAdapter(Context mContext, ArrayList<Payment> listPayment) {
		this.mContext = mContext;
		this.listPayment = listPayment;
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		private TextView tvEmploy, tvAmount, tvType, tvCurency, tvNote;

		public ViewHolder(View itemView) {
			super(itemView);
			tvEmploy = (TextView) itemView.findViewById(R.id.tv_payment_employee);
			tvAmount = (TextView) itemView.findViewById(R.id.tv_payment_amount);
			tvType = (TextView) itemView.findViewById(R.id.tv_payment_type);
			tvCurency = (TextView) itemView.findViewById(R.id.tv_payment_currency);
			tvNote = (TextView) itemView.findViewById(R.id.tv_payment_note);
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.item_payment, parent, false);
		RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100);
		layoutParams.setMargins(0, 2, 0, 2);
		view.setLayoutParams(layoutParams);
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		ViewHolder viewHolder = (ViewHolder) holder;
		viewHolder.tvEmploy.setText(listPayment.get(position).getEmployee());
		viewHolder.tvAmount.setText(listPayment.get(position).getAmount());
		viewHolder.tvType.setText(listPayment.get(position).getType());
		viewHolder.tvCurency.setText(listPayment.get(position).getCurrency());
		viewHolder.tvNote.setText(listPayment.get(position).getNote());
	}

	@Override
	public int getItemCount() {
		return listPayment.size();
	}
}
