package com.anhtuan.transportation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anhtuan.transportation.R;
import com.anhtuan.transportation.information.Partner;

import java.util.ArrayList;

/**
 * Created by ${ADMIN} on 03/18/17.
 */
public class PartnerAdapter extends RecyclerView.Adapter {
	private Context mContext;
	private ArrayList<Partner> listPartner;
	public OnClickItemPartner onClickItemPartner;

	public void setOnClickItemPartner(OnClickItemPartner onClickItemPartner) {
		this.onClickItemPartner = onClickItemPartner;
	}

	public PartnerAdapter(Context mContext, ArrayList<Partner> listPartner) {
		this.mContext = mContext;
		this.listPartner = listPartner;
	}

	private class ViewHolder extends RecyclerView.ViewHolder {
		private LinearLayout lnPartner;
		private TextView tvName, tvPhoneNumber, tvFrom, tvTo, tvStatus;

		public ViewHolder(View itemView) {
			super(itemView);
			lnPartner = (LinearLayout) itemView.findViewById(R.id.ln_content_partner);
			tvName = (TextView) itemView.findViewById(R.id.tv_partner_name);
			tvPhoneNumber = (TextView) itemView.findViewById(R.id.tv_part_mobile);
			tvFrom = (TextView) itemView.findViewById(R.id.tv_partner_from);
			tvTo = (TextView) itemView.findViewById(R.id.tv_partner_to);
			tvStatus = (TextView) itemView.findViewById(R.id.tv_partner_status);
			lnPartner.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onClickItemPartner.showDetailPartner(getAdapterPosition());
				}
			});
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View view = LayoutInflater.from(mContext).inflate(R.layout.item_partner, parent, false);
		RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
		layoutParams.setMargins(0, 2, 0, 2);
		view.setLayoutParams(layoutParams);
		return new PartnerAdapter.ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		final PartnerAdapter.ViewHolder viewHolder = (PartnerAdapter.ViewHolder) holder;
		viewHolder.tvName.setText(listPartner.get(position).getName());
		viewHolder.tvPhoneNumber.setText(listPartner.get(position).getMobile());
		viewHolder.tvFrom.setText(listPartner.get(position).getFrom());
		viewHolder.tvTo.setText(listPartner.get(position).getTo());
		viewHolder.tvStatus.setText(listPartner.get(position).getStatus());
	}

	@Override
	public int getItemCount() {
		return listPartner.size();
	}

	public interface OnClickItemPartner {
		void showDetailPartner(int position);
	}
}
