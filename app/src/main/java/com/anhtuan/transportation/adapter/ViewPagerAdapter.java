package com.anhtuan.transportation.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${ADMIN} on 03/16/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
	List<Fragment> mFragment = new ArrayList<>();
	List<String> mTitle = new ArrayList<>();

	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mTitle.get(position);
	}

	@Override
	public Fragment getItem(int position) {
		return mFragment.get(position);
	}

	@Override
	public int getCount() {
		return mFragment.size();
	}

	public void addFragment(Fragment fragment, String title) {
		mFragment.add(fragment);
		mTitle.add(title);
	}
}
